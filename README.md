## Agenda

1. whoami
    - Mateusz Kubaszek
    - Architekt, programista i mlopsowiec
    - Mąż, padre
    - Ewangelista Rusta i optymalizacji
    - Systemy rozproszone, algorytmy i FP
2. Nasz problem!
    - Zdefiniujmy
    - Wymagania funkcjonalne i niefunkcjonalne
3. Czym jest Event Sourcing?
    - Krótki wstęp
    - Zrobienie protego flow
4. Jak przenieść ES do kodu?
    - Metodologia Kaizen i DDD technicznego
5. Jak zrobić szybko i sprawnie Modularny Monolit?
    - Trochę o architekturze
6. Trochę MLa
    - Robimy klasyfikator
    - Zapisujemy model
7. Deploy i uwagi
    - Uruchamiamy wszystko!!!
